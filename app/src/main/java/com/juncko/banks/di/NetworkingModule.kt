package com.juncko.banks.di

import com.juncko.banks.data.api.BanksApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val BASE_URL = "https://api.myjson.com"
const val CONNECTION_TIMEOUT = 300L

fun providesHttpClient() : OkHttpClient {

    val interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    return OkHttpClient().newBuilder().connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS).addInterceptor(interceptor).addInterceptor {
        var request = it.request()
        val builder = request.newBuilder().addHeader("Content-Type","application/json").method(request.method, request.body)
        it.proceed(builder.build())
    }.build()
}

fun providesRetrofit(httpClient: OkHttpClient) : Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

val networkingModule = module {
    single { providesHttpClient() }
    single { providesRetrofit(get())}
    single { get<Retrofit>().create(BanksApiService::class.java) }
}