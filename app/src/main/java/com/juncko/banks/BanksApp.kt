package com.juncko.banks

import android.app.Application
import com.juncko.banks.di.networkingModule
import com.juncko.banks.ui.banklist.di.BanksModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BanksApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@BanksApp)
            modules(listOf(networkingModule,BanksModule))
        }

        if (BuildConfig.DEBUG) System.setProperty("kotlinx.coroutines.debug", "on")
    }
}