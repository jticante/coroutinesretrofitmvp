package com.juncko.banks.ui.banklist.di

import com.juncko.banks.data.model.Bank

interface BanksContract {
    interface View {
        fun showError(error : String)
        fun showLoader(show : Boolean)
        fun displayBanks(banks : List<Bank>)
    }

    interface Presenter {
        fun getBanks()
    }

    interface Repository {
        suspend fun getBanks(): List<Bank>
    }
}