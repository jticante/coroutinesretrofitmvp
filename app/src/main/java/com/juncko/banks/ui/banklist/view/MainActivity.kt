package com.juncko.banks.ui.banklist.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.juncko.banks.R
import com.juncko.banks.data.model.Bank
import com.juncko.banks.ui.banklist.di.BanksContract
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity(), BanksContract.View {

    val banksPresenter : BanksContract.Presenter by inject { parametersOf(this) }
    private lateinit var banksAdapter: BanksListAdapter
    private var banksRest  = mutableListOf<Bank>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        banksAdapter = BanksListAdapter(emptyList())
        rvbanks.apply {
            adapter = banksAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

    }

    override fun onStart() {
        super.onStart()
        banksPresenter.getBanks()
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showLoader(show: Boolean) {
        Log.d("BankosLoader", "loader ${show}")
    }

    override fun displayBanks(banks: List<Bank>) {
        banksRest.clear()
        banksRest = banks.toMutableList()
        banksAdapter.updateDataSet(banks)
    }
}
