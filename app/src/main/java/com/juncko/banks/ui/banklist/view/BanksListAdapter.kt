package com.juncko.banks.ui.banklist.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.juncko.banks.R
import com.juncko.banks.data.model.Bank
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_banks.view.*

class BanksListAdapter(var banks : List<Bank>):RecyclerView.Adapter<BanksListAdapter.BankViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BankViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_banks, parent, false)
    )

    override fun getItemCount() = banks.size

    override fun onBindViewHolder(holder: BanksListAdapter.BankViewHolder, position: Int) = holder.bind(banks[position])

    public fun updateDataSet(banks: List<Bank>){
        this.banks = banks
        notifyDataSetChanged()
    }

    inner class BankViewHolder(view : View): RecyclerView.ViewHolder(view){
        private val layout = view.banksLayout
        private val name = view.txtname
        private val description = view.txtDescription
        private val age = view.txtage
        private val imgBank = view.imgLogo
        fun bind(bank : Bank){
            name.text = bank.bankName
            description.text = bank.description
            age.text = "${bank.age}"
            Picasso.get().load(bank.url).into(imgBank)
        }
    }
}



