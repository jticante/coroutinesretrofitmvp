package com.juncko.banks.ui.banklist

import android.util.Log
import com.google.gson.Gson
import com.juncko.banks.data.api.BanksApiService
import com.juncko.banks.data.model.Bank
import com.juncko.banks.ui.banklist.di.BanksContract

class BanksRepositoryImpl(private val banksApiService: BanksApiService) : BanksContract.Repository  {
    override suspend fun getBanks(): List<Bank> {
        val banks = try {
            banksApiService.getBanks()
        }catch (error: Throwable) {
            print(error.localizedMessage)
            null
        }

        return banks!!
    }

}