package com.juncko.banks.ui.banklist

import android.content.Context
import android.util.Log
import com.juncko.banks.ui.banklist.di.BanksContract
import com.juncko.banks.utils.logCoroutine
import kotlinx.coroutines.*

class BanksPresenterImpl(val context: Context,
                         val view : BanksContract.View,
                         val repository : BanksContract.Repository) : BanksContract.Presenter{
    var job : Job? = null
    val exceptionHandler = CoroutineExceptionHandler{
            coroutineContext, throwable -> throwable.printStackTrace()
    }

    private val TAG : String = BanksPresenterImpl::class.java.simpleName

    override fun getBanks() {
        view.showLoader(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val result = runCatching {
                repository.getBanks()
            }
            withContext(Dispatchers.Main){
                result.onSuccess { banks ->
                    view.displayBanks(banks)
                    view.showLoader(false)
                }.onFailure { error ->
                    handleError(error)
                    view.showLoader(false)
                }
            }
        }
    }

    private fun handleError(throwable: Throwable) {
        view.showError(throwable.localizedMessage)
    }
}