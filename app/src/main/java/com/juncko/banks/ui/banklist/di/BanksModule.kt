package com.juncko.banks.ui.banklist.di

import com.juncko.banks.contextProvider.CoroutineContextProvider
import com.juncko.banks.contextProvider.CoroutineContextProviderImpl
import com.juncko.banks.ui.banklist.BanksPresenterImpl
import com.juncko.banks.ui.banklist.BanksRepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val BanksModule = module {
    single { CoroutineContextProviderImpl(Dispatchers.IO) as CoroutineContextProvider }
    single<BanksContract.Repository> { BanksRepositoryImpl(get()) }
    factory<BanksContract.Presenter> { (lv : BanksContract.View) ->
        BanksPresenterImpl(get(), lv, get())
    }
}