package com.juncko.banks.data.api

import com.juncko.banks.data.model.Bank
import retrofit2.http.GET

interface BanksApiService {
    @GET("/bins/19e11s")
    suspend fun getBanks(): List<Bank>
}