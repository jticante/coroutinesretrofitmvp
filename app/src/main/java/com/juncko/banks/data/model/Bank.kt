package com.juncko.banks.data.model

import com.google.gson.annotations.SerializedName

data class Bank(@SerializedName("description")
                val description: String = "",
                @SerializedName("bankName")
                val bankName: String = "",
                @SerializedName("age")
                val age: Int = 0,
                @SerializedName("url")
                val url: String = "")